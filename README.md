# Overview

Nodejs module - adapted version of [`util.js` library](https://bitbucket.org/workslon/ontojs/src/8367a54d7dbdc4330a750dfdfbbd9a3e27e17867/lib/util.js) written by Prof. Gerd Wagner, BTU Cottbus

You can find its source code [here](https://bitbucket.org/workslon/gw-util/src/f1b8b412a84e3b297c638452c0ccd92d46c5bb4a/index.js)

# Install

## ssh

```
npm install git+ssh://git@bitbucket.org/workslon/gw-util.git --save
```

## https

```
npm install https://git@bitbucket.org/workslon/gw-util.git --save
```

# Usage

## CommonJS

```javascript
var util = require('gw-util');

var randomInt = util.randomInt(0, 10);
var userLang = util.getUserLanguage();

```

## ES6 Modules

### Load whole library

```javascript
import util from 'gw-util';

var randomInt = util.randomInt(0, 10);
var userLang = util.getUserLanguage();
```

### Get required methods only

```javascript
import { randomInt, userLang } from 'gw-util';

let randomInt = randomInt(0, 10);
let userLang = getUserLanguage();
```